<?php

/**
 * @file
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function uw_custom_frontend_preprocess_html(&$variables) {
  global $user;
  global $base_path;

  // Added variable to set drupal header status (mainly to check for 404s).
  $variables['header_status'] = drupal_get_http_header('status');

  // Added variable for publication theme.
  $variables['publication_theme'] = variable_get('publication_theme', 'publication');
  $variables['publication_colorBar'] = variable_get('uw_fdsu_theme_color_css', 'org_default');
  // Add uWaterloo global google analytics account.
  $ga = "<script>";
  if (variable_get('uw_cfg_google_analytics_account') || variable_get('google_analytics_enable') == 1) {
    $ga .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" .
        " (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" .
        "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" .
        " })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n";
  }
  if (!(variable_get('uw_cfg_google_analytics_account')) && variable_get('google_analytics_enable') == 1) {
    // Removed in favour of Google Tag Manager.
    // $ga .= "ga('create', 'UA-51776731-1', 'auto');\n";
    // $ga .= "ga('send', 'pageview');\n";
  }
  if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 0) {
    $ga .= "ga('create', '" . check_plain(variable_get('uw_cfg_google_analytics_account')) . "', 'auto');\n";
    $ga .= "ga('send', 'pageview');\n";
  }
  if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 1) {
    // Removed in favour of Google Tag Manager.
    // $ga .= "ga('create', 'UA-51776731-1', 'auto');\n";
    // New tracker.
    $ga .= "ga('create', '" . check_plain(variable_get('uw_cfg_google_analytics_account')) . "', 'auto', {'name': 'newTracker'});\n";
    $ga .= "ga('send', 'pageview');\n";
    // Send page view for new tracker.
    $ga .= "ga('newTracker.send', 'pageview');\n";
  }
  $ga .= "</script>";

  $variables['ga'] = $ga;

  // Add Google Tag Manager snippets.
  if (module_enable(array('google_tag'))) {
    module_load_include('inc', 'google_tag', 'includes/admin');
    module_load_include('inc', 'google_tag', 'includes/snippet');
    $variables['gtm'] = google_tag_snippets();
  }

  // Add Login link.
  // Note that target="_self" is needed so Angular makes the link work properly by forcing a refresh.
  if ($user->uid != 0) {
    $variables['login_link'] = "<a class='login-link' href='" . $base_path . "caslogout?destination=/' target='_self'>Log out</a>";
    if (user_access('access workbench')) {
      $variables['login_link'] = t('<a class="login-link" href="@url" target="_self">My workbench</a>', array('@url' => url('admin/workbench'))) . ' &nbsp; ' . $variables['login_link'];
    }
  }
  else {
    $variables['login_link'] = "<a class='login-link' href='" . $base_path . "cas?destination=admin/workbench' target='_self'>Log in</a>";
  }

  $js = array();
  // Calling all modules implementing hook_uw_publication_add_js.
  foreach (module_implements('uw_publication_add_js') as $module) {
    // Get return value for this module.
    $urls = module_invoke($module, 'uw_publication_add_js');
    // Don't do anything if blank.
    if ($urls) {
      // If a single value was sent instead of an array, convert to a single-item array.
      if (!is_array($urls)) {
        $urls = array($urls);
      };
      // Loop through all the URLs
      foreach ($urls as $url) {
        $url = $base_path . $url;
        // TODO: validate that the file exists before adding it to the array.
        $js[] = $url;
      }
    }
  }
  $variables['add_js'] = $js;
  
  $css = array();
  // Calling all modules implementing hook_uw_publication_add_css.
  foreach (module_implements('uw_publication_add_css') as $module) {
    // Get return value for this module.
    $urls = module_invoke($module, 'uw_publication_add_css');
    // Don't do anything if blank.
    if ($urls) {
      // If a single value was sent instead of an array, convert to a single-item array.
      if (!is_array($urls)) {
        $urls = array($urls);
      };
      // Loop through all the URLs
      foreach ($urls as $url) {
        $url = $base_path . $url;
        // TODO: validate that the file exists before adding it to the array.
        $css[] = $url;
      }
    }
  }
  $variables['add_css'] = $css;

}
