<?php

/**
 * @file
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function uw_custom_frontend_form_system_theme_settings_alter(&$form, $form_state) {

  $form['color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color scheme'),
  );

  $form['color']['css'] = array(
    '#type' => 'select',
    '#title' => t('Select a color scheme to use for the colour bar'),
    '#default_value' => variable_get('uw_fdsu_theme_color_css', 'org_default'),
    '#options' => array(
      'org_default' => t('Default (yellow)'),

      'org_art' => t('Arts (orange)'),
      'org_eng' => t('Engineering (purple)'),
      'org_env' => t('Environment (green)'),
      'org_ahs' => t('Health (teal)'),
      'org_mat' => t('Mathematics (pink)'),
      'org_sci' => t('Science (blue)'),

      'org_cgc' => t('Conrad Grebel University College (red)'),
      'org_ren' => t('Renison University College (red)'),
      'org_stj' => t('St. Jerome’s University (red)'),
      'org_stp' => t('St. Paul’s University College (red)'),

      'org_school' => t('School (red)'),

      'none' => t('No colour bar (permission required)'),
    ),
  );

  $form['#submit'][] = 'uw_custom_frontend_set_custom_variable';

}

/**
 *
 */
function uw_custom_frontend_set_custom_variable($form, &$form_state) {
  if (isset($form_state['values']['css'])) {
    variable_set('uw_fdsu_theme_color_css', $form_state['values']['css']);
  }
}
